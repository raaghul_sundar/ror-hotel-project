Rails.application.routes.draw do
  get 'bookings/index'
  devise_for :users
  get 'welcome/index'
  get 'afavourites/create' => "afavourites#create"
  #post '/afavourites/create', to: "afavourites#create"
  put "roles/:id/activate" => "roles#activate", :as => "active_role"
  #post 'afavourites/:id/build' => "afavourites#build", as: 'build_afavourites'
  resources :hotels do
  	resources :comments
  	resources :afavourites
    resources :bookings
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index'
end

