class CreateAfavourites < ActiveRecord::Migration[5.2]
  def change
    create_table :afavourites do |t|
      t.integer :userid
      t.string :hotelid
      t.references :user, foreign_key: true
      t.references :hotel, polymorphic: true

      t.timestamps
    end
  end
end
