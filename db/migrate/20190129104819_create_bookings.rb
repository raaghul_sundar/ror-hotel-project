class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.integer :user_id
      t.integer :hotel_id
      t.string :slot
      t.integer :seat

      t.timestamps
    end
  end
end
