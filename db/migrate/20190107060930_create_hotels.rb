class CreateHotels < ActiveRecord::Migration[5.2]
  def change
    create_table :hotels do |t|
      t.string :name
      t.string :location
      t.string :hoteltype
      t.string :phone
      t.string :cuisins
      t.decimal :avgcost
      t.string :address
      t.string :features
      t.string :special
      t.integer :userid

      t.timestamps
    end
  end
end
