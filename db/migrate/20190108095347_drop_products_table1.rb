class DropProductsTable1 < ActiveRecord::Migration[5.2]
  def change
  	def up
  		if table_exists?(:favourites)
         drop_table :favourites
        end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
  end
end
