class AddImageToComments < ActiveRecord::Migration[5.2]
  def change
  	add_column :comments, :commenter_image, :string
  end
end
