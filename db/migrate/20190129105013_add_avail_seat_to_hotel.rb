class AddAvailSeatToHotel < ActiveRecord::Migration[5.2]
  def change
  	add_column :hotels, :slot1_avail_seat, :integer, :default => 30
  	add_column :hotels, :slot2_avail_seat, :integer, :default => 30
  end
end
