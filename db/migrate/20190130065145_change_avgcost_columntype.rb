class ChangeAvgcostColumntype < ActiveRecord::Migration[5.2]
  def change
  	change_column :hotels, :avgcost, :float
  end
end
