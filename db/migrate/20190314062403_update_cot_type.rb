class UpdateCotType < ActiveRecord::Migration[5.2]
  def change
    change_column :bookings, :cot, :string
  end
end
