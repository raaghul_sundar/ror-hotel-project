class AddPhoneToBookings < ActiveRecord::Migration[5.2]
  def change
  	add_column :bookings, :phone_main, :string
  	add_column :bookings, :phone_alt, :string
  	add_column :bookings, :email, :string, :default => "", :null => false
  end
end
