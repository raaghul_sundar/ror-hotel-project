class BookingMailer < ApplicationMailer
	default :from => "raaghulfake98@gmail.com"

   def booking_email(user, hotel)
   	@user = user
   	@hotel = hotel
    mail(:to => "#{user.fname} <#{user.email}>", :subject => "Booking Confirmation Mail")
   end
end
