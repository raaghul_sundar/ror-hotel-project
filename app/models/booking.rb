class Booking < ApplicationRecord
	belongs_to :hotel
	belongs_to :user, optional: true

	validates :slot, :seat, :phone_main, presence: true
	validate :check_length
	after_create :send_email

	def send_email
		BookingMailer.booking_email(self.user, self.hotel).deliver
	end

	def check_length
		unless phone_main.size == 10 or phone_alt.size == 10
		  errors.add(:phone_main, "please enter a valid phone number") 
		end
	end
end
