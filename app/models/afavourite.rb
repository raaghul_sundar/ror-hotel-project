class Afavourite < ApplicationRecord

  belongs_to :hotel, optional: true
  belongs_to :user, optional: true, dependent: :destroy
  #attr_accessor :userid, :hotelid
  validates :userid, :hotelid, presence: true
end
