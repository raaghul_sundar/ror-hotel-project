class Hotel < ApplicationRecord
	has_many :users, :through => :afavourite
	has_many :comments, dependent: :destroy
	has_many :afavourites
	has_many :bookings, dependent: :destroy
	
	mount_uploader :image, ImageUploader
	
	validates :name, :location, :phone, :avgcost, :address, presence: true
	validates :image, file_size: { less_than: 1.megabytes }
	

end
