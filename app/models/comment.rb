class Comment < ApplicationRecord
  belongs_to :hotel
  mount_uploader :commenter_image, ImageUploader
  validates :commenter_image, file_size: { less_than: 1.megabytes }
end
