class HotelsController < ApplicationController
  def index
    @hotels = Hotel.all
    @users = User.all
    @afavourites = Afavourite.find_by(params[:afavourite_id] || params[:id])
  end

  def show
    @hotel = Hotel.find_by_id(params[:id] || params[:hotel_id])
  end

  def new
  	@hotel = Hotel.new
  end

  def edit
    @hotel = Hotel.find(params[:id])
  end

  def create
    @hotel = Hotel.new(hotel_params)
    @hotel.userid = current_user.id
    if @hotel.save
      redirect_to @hotel
    else
      render 'new'
    end
  end

def update
  @hotel = Hotel.find(params[:id])
  if @hotel.update(hotel_params)
    redirect_to @hotel
  else
    render 'edit'
  end
end

def destroy
  @hotel = Hotel.find(params[:id])
  @hotel.destroy
  redirect_to @hotel
end

  private
    def hotel_params
      params.require(:hotel).permit(:name, :location, :hoteltype, :phone, :cuisins, :avgcost, :address, :features, :special, :image, :image_cache, :remove_image, :slot1_avail_seat, :slot2_avail_seat) 
    end
end
