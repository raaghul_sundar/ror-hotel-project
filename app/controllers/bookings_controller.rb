class BookingsController < ApplicationController
	before_action :set_hotel
  def index
  	@bookings = Booking.where(user_id: current_user.id, hotel_id: @hotel.id)
  end

  def new
  	@booking = Booking.new
  end

  def create
    @booking = @hotel.bookings.new(booking_params)
    @booking.user_id = current_user.id
    @booking.hotel_id = @hotel.id
    @booking.email = current_user.email
    $avail_seat = params[:booking][:seat].to_i
  	if params[:booking][:slot] == "Afternoon Slot"
	  	if 	@hotel.slot1_avail_seat != nil && $avail_seat != 0
		    if @hotel.slot1_avail_seat < $avail_seat
		    	redirect_to new_hotel_booking_path(@hotel), flash: {alert: "Sorry..only #{@hotel.slot1_avail_seat} seat available"}
			  else
				  @hotel.update_attributes(:slot1_avail_seat => @hotel.slot1_avail_seat - $avail_seat)
          @hotel.save
          redirect_to hotel_bookings_path(@hotel), notice: "Your Booking has been Accepted" if @booking.save!     
		   end
		  else
        flash[:alert] = "Sorry..bookings already filled." 
		  	redirect_to hotels_path(@hotel)
	  	end
  	elsif params[:booking][:slot] == "Evening Slot"
  		if 	@hotel.slot2_avail_seat != nil
  		    if @hotel.slot2_avail_seat < $avail_seat
  		    	redirect_to new_hotel_booking_path(@hotel), notice: "Sorry..only #{@hotel.slot2_avail_seat} seat available"
  			  else
  				  @hotel.update_attributes(:slot2_avail_seat => @hotel.slot2_avail_seat - $avail_seat)
  		      @hotel.save
            redirect_to hotel_bookings_path(@hotel), notice: "Your Booking has been Accepted" if @booking.save!     
          end
  		else
  		  	redirect_to hotel_path(@hotel), notice: "Sorry..bookings already filled."
  	  end 		
  	end
  end

  def destroy
  	@booking = @hotel.bookings.find(params[:id] || params[:booking_id])
  	if @booking.slot == "Afternoon Slot"
	  	@hotel.slot1_avail_seat = @hotel.slot1_avail_seat + @booking.seat
      @hotel.save
  	elsif @booking.slot == "Evening Slot"
      @hotel.slot2_avail_seat = @hotel.slot2_avail_seat + @booking.seat
      @hotel.save
  	end
  	redirect_to hotel_path(@hotel), notice: "Your Booking Cancelled Successfully" if @booking.destroy!
  end

   private
  def booking_params
  	params.require(:booking).permit(:user_id, :hotel_id, :slot, :seat, :phone_main, :phone_alt, :email)
	end
  def set_hotel
    @hotel = Hotel.find_by_id(params[:hotel_id])
    @user = User.find_by_id(params[:user_id])
  end
end
