class AfavouritesController < ApplicationController
	 def index
		@afavourite = Afavourite.where(userid: current_user.id)
	 end

	def create
		@hotel = Hotel.find_by_id(params[:hotel_id])
	    #if current_user.afavourites.exists?(:hotelid => @hotel.id)
	    if 	Afavourite.exists?(userid: current_user.id, hotelid: @hotel.id)
	          redirect_to hotel_afavourites_path(:hotelid, :id), alert: 'This hotel is already in your favourite list'
	    else
	    	  @afavourite = @hotel.afavourites.create({userid: current_user.id, hotelid: params[:hotel_id]})
		      @afavourite.save
		      if @afavourite.save
		      redirect_to hotel_path(@hotel), notice: 'Hotel has been favorited'
		      else
		      redirect_to @hotel, alert: 'Something went wrong...*sad panda*'
		      end
	    end
	end

	def destroy
	    @hotel = Hotel.find_by_id(params[:hotel_id])
	    @afavourite =  @hotel.afavourites.find_by_id(params[:afavourite_id] || params[:id])
	    @afavourite.destroy if !@afavourite.nil?
	    redirect_to hotel_path(@hotel), notice: 'Hotel is no longer in your favorites'
	end

 	def new
	 	@afavourite = Afavourite.new
	end
end
