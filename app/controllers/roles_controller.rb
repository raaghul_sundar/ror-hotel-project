class RolesController < ApplicationController
	def activate
	  @user = User.find(params[:id])
	  if @user.update_attribute(:role, 'member')
	    redirect_to root_path, notice: 'Registered as Member successfully.'
	  else
	   	render welcome_index_path
	  end 
	end
end
