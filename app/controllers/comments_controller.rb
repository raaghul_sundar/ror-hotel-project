class CommentsController < ApplicationController
	def create
    @hotel = Hotel.find(params[:hotel_id])
    @comment = @hotel.comments.create(comment_params)
    @comment.commenter = current_user.fname
    @comment.save
    redirect_to hotel_path(@hotel)
  end
 
 def destroy
    @hotel = Hotel.find(params[:hotel_id])
    @comment = @hotel.comments.find(params[:id])
    @comment.destroy
    redirect_to hotel_path(@hotel)
  end

  private
    def comment_params
      params.require(:comment).permit(:commenter, :body, :commenter_image, :commenter_image_cache, :remove_commenter_image)
    end
end
